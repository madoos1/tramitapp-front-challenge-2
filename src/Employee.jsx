import React from 'react';

const Employee = ({ firstName, lastName, style }) => <div className="person" style={style}><span >{firstName} {lastName}</span></div>

export default Employee