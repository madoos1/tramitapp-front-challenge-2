// includesInLowerCase :: (String, String) -> Boolean
export const includesInLowerCase = (x, y) => x.toLowerCase().includes(y.toLowerCase())