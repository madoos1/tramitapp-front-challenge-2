import React from 'react'
import { List, WindowScroller } from 'react-virtualized';
import 'react-virtualized/styles.css';

const VirtualList = ({ width, items, children }) => {
  return (
    <WindowScroller>
        {
          ({ height, isScrolling, onChildScroll, scrollTop }) => 
          <List
          autoHeight
          height={height}
          scrollTop={scrollTop}
          isScrolling={isScrolling}
          onScroll={onChildScroll}
          rowCount={items.length}
          width={width}
          rowHeight={45}
          rowRenderer={({key, index, style}) => children(items[index], key, style)}
        />
            
      }
    </WindowScroller>

  );
}

export default VirtualList;

