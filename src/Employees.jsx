import React from 'react';
import Employee from './Employee'
import VirtualList from './VirtualList'

const Employees = ({ employees }) => {
  return (
    <div className="people">
      <VirtualList items={employees} width={288}>
        {(item, key, style) => <Employee key={key} {...item} style={style} />}
      </VirtualList>
    </div>
  );
}



export default Employees;
