import React from 'react';
import './App.css';
import Week from './Week.jsx';
import Columns from './Columns.jsx';
import EmployeeEventsRow from './EmployeeEventsRow.jsx';
import VirtualList from './VirtualList'


const GanttChart = ({ employees, weeklyCalendar, handleOpenModal, handleUpdate }) => {
  return (
    <div className="chart">
      <div className="content">
          <Week weeklyCalendar={weeklyCalendar}></Week>
      </div>
      <Columns weeklyCalendar={weeklyCalendar} employees={employees} handleUpdate={handleUpdate}></Columns>
      <div className="rows">
        
      <VirtualList items={employees} width={2000}>
        {(item, key, style) => 
            <EmployeeEventsRow 
              key={key} 
              style={style} 
              employee={item} 
              handleOpenModal={handleOpenModal} 
              calendar={weeklyCalendar}
            />}
      </VirtualList>

      </div>
    </div>
  );
}

export default GanttChart;
