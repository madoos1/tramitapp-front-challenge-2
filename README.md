# TramitApp Front-End challenge #2

En esta página mostramos 6000 empleados a la vez junto con eventos de algunos de los días. La carga de la página es lenta y además al filtrar por nombre, también se ve bastante lento. ¿Por qué? Realiza una solución para poder mostrar los datos de los 6.000 empleados de forma que el performance no sufra. (No se puede paginar, queremos mostrar los 6000 empleados en la misma página)

## Iniciar APP

* `npm install`
* `npm start`
* Se pueden ver los cambios implementados haciendo click [aquí](https://bitbucket.org/madoos1/tramitapp-front-challenge-2/compare/master%0Dchallenge#diff) 
## Solución

La APP va lenta porque se está intentando renderizar 6000 items a la vez, esto es demasiado para el GPU del browser. La técnica que he usado para solucionar los problemas de performance es virtual rendering. La idea básica de “VR” es renderizar solo lo que ve el usuario, manteniendo el número de elementos renderizados al mínimo.

Para ese propósito he implementado un HOC llamado VirtualList utilizando react-virtualized

Este componente permite renderizar una larga lista de elementos sin afectar al performance.

Ejemplo de uso:

```js
import React from 'react';
import Employee from './Employee'
import VirtualList from './VirtualList'

const Employees = ({ employees }) => {
  return (
    <div className="people">
      <VirtualList items={employees} width={288}>
        {(employee, key, style) => 
            <Employee 
                firstName={employee.firstName}
                lastName={employee.lastName}
                key={key} 
                style={style} 
            />}
      </VirtualList>
    </div>
  );
}

export default Employees;
```

Para realizar la solución he refactorizado los componentes Employees y GanttChart convirtiéndolos en componentes puros porque contenían lógica duplicada de filtrado. Esa lógica se ha movido al componente mediador (en este caso APP) porque de esa forma el app se puede mantener mejor y se evita duplicidad en el código. También he actualizado la versión de React y solucionado los errores del linter.